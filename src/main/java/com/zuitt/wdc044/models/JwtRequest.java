package com.zuitt.wdc044.models;

import java.io.Serializable;

public class JwtRequest implements Serializable {
    // properties
    private static final long serialVersionUID = 59264685300515787L;

    private String username;

    private String password;

    // constructors
    public JwtRequest(){}

    public JwtRequest(String username, String password){
        this.setUsername(username);
        this.setPassword(password);
    }

    // setters and getters
    public String getUsername(){
        return this.username;
    }

    public String setUsername(String username){
        return this.username = username;
    }

    public String getPassword(){
        return this.password;
    }

    public String setPassword(String password){
        return this.password = password;
    }
}
